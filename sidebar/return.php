
<?php
include ("menu.php");
//search
ini_set('display_errors', 1);
error_reporting(~0);

$strKeyword = null;

if (isset($_POST["txtKeyword"])) {
    $strKeyword = $_POST["txtKeyword"];
}
?>
<form style="float:right;" name="frmSearch" method="post" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>">
<div class="form-group" >
			<div class="input-group" >

					<input name="txtKeyword" type="text" id="txtKeyword"  value="<?php echo $strKeyword; ?>">
					<input type="submit" value="Search" class="btn btn-dark"></th>
				
					</div>
			</div>


</form>



<?php

include '../config/config.php';
//search





$sql = "SELECT br.*, pt.producttype_name,pd.product_name,member_firstname FROM borrow as br
inner JOIN producttype AS pt ON (br.producttype_id = pt.producttype_id)
inner JOIN product AS pd ON (br.product_id = pd.product_id)
inner JOIN member AS mb ON (br.member_id = mb.member_id)
WHERE borrow_status = '0' and product_name LIKE '%" . $strKeyword . "%'"; 



$query = mysqli_query($conn, $sql);


?>
<h3>คืนอุปกรณ์</h3>


	<div class="well" style="margin:auto; padding:auto; width:100%;">
		<div style="height:20px;"></div>
		<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover">
			<thead>
				<th>ชื่อผู้ยืม</th>
                <th>ชื่ออุปกรณ์</th>
                <th>ประเภทอุปกรณ์</th>
                <th>จำนวนอุปกรณ์ที่ยืม</th>
                
                <th>กำหนดวันคืน</th>
                <th>สถานะ</th>
                <th>Action</th>
				
			</thead>
			<tbody>
			<?php

while ($row = mysqli_fetch_array($query)) {

    ?>
        <?php
        if ($row['borrow_status'] == "0") {
        $borrow_status = "ยังไม่คืน";
        } else if ($row['borrow_status'] == "1") {
        $borrow_status = "คืนแล้ว";
        } else {
        
        }
        ?>

					<tr>
						<td><?php echo $row['member_firstname'] ?></td>
                        <td><?php echo $row['product_name']; ?></td>
                        <td><?php echo $row['producttype_name']; ?></td>
                        <td><?php echo $row['borrow_amount']; ?></td>
                        <td><?php echo $row['return_date']; ?></td>
                        <td><?php echo $borrow_status; ?></td>
                    <td>
						<div align="center">
                        <a href="#edit<?php echo $row['borrow_id']; ?>" data-toggle="modal" class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span><i class="fas fa-edit"></i> คืน</a> &nbsp;	
    
                      <div class="modal fade" id="edit<?php echo $row['borrow_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" enctype="multipart/form-data">
                        <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                        <div class="modal-header">
	                    <h4 align="center" class="modal-title" id="myModalLabel">การคืน</h4>
	                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                             <p class="modal-body">
				            <?php

	$edit = mysqli_query($conn, "SELECT br.*, pt.producttype_name,pd.product_name,member_firstname FROM borrow as br
    inner JOIN producttype AS pt ON (br.producttype_id = pt.producttype_id)
    inner JOIN product AS pd ON (br.product_id = pd.product_id)
    inner JOIN member AS mb ON (br.member_id = mb.member_id)
    WHERE borrow_id ='" . $row['borrow_id'] . "'");
	$erow = mysqli_fetch_array($edit);
	
    
    ?>
				<div class="container-fluid">
				<form method="POST" action="../process/process_action.php?p=return&borrow_id=<?php echo $row['borrow_id']; ?>" enctype="multipart/form-data"> <!-- form -->
				<div class="row">
                    
						<div class="col-lg-2">
							<label style="position:relative; top:7px;">ชื่ออุปกรณ์:</label>
						</div>
						<div class="col-lg-10">
                        <label ><?php echo $erow['product_name']; ?></label>
							<input type="hidden" name="product_name" class="form-control" value="<?php echo $erow['product_id']; ?>"required>
                        </div>
                        <div class="col-lg-2">
							<label style="position:relative; top:7px;">ชื่อประเภทอุปกรณ์:</label>
						</div>
						<div class="col-lg-10">
                            <label ><?php echo $erow['producttype_name']; ?></label>
							<input type="hidden" name="producttype_id" class="form-control" value="<?php echo $erow['producttype_id']; ?>"required>
                        </div>
                        <div class="col-lg-2">
							<label style="position:relative; top:7px;">จำนวนอุปกรณ์ที่คืน:</label>
						</div>
						<div class="col-lg-10">
                            <input type="text" name="return_amount" class="form-control" value="<?php echo $erow['borrow_amount']; ?>"required>
                        </div>
                        <div class="col-lg-2">
							<label style="position:relative; top:7px;">กำหนดวันที่คืน: </label>
						</div>
						<div class="col-lg-10">
                            <label><?php echo $erow['return_date']; ?></label>
                            <input type="date" hidden="hidden" name="return_date" class="form-control" value="<?php echo $erow['return_date']; ?>"required>
						</div>
                    </div>
                
                <div class="modal-footer">
                    <button type="submit" class="btn btn-warning"><span class="glyphicon glyphicon-check"></span> ยืนยันการคืน</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> ยกเลิก</button>    
                </div>
				</form>
            </div>
        </div>
    </div>
   
   

						</td>
					</tr>
					<?php
}

?>
			</tbody>
		</table>
		</div>   
                 
    