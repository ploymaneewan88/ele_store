<?php session_start(); 
include ("menu.php");
include '../config/config.php';

$meSql = "SELECT pd.*, pt.producttype_name FROM product as pd
inner JOIN producttype AS pt ON (pd.producttype_id = pt.producttype_id)";
$meQuery = mysqli_query($conn,$meSql );

$action = isset($_GET['a']) ? $_GET['a'] : "";
$itemCount = isset($_SESSION['cart']) ? count($_SESSION['cart']) : 0;
if(isset($_SESSION['qty'])){
    $meQty = 0;
    foreach($_SESSION['qty'] as $meItem){
        $meQty = $meQty + $meItem;
    }
}else{
    $meQty=0;
}
//search
ini_set('display_errors', 1);
error_reporting(~0);

$strKeyword = null;

if (isset($_POST["txtKeyword"])) {
    $strKeyword = $_POST["txtKeyword"];
}
?>
<form style="float:right;" name="frmSearch" method="post" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>">
<div class="form-group" >
			<div class="input-group" >

					<input name="txtKeyword" type="text" id="txtKeyword"  value="<?php echo $strKeyword; ?>">
					<input type="submit" value="Search" class="btn btn-dark"></th>
				
					</div>
			</div>


</form>



<?php


//search


?>
<h3>รายการอุปกรณ์</h3>
<?php
if($action == 'exists'){
    echo "<div class=\"alert alert-warning\">เพิ่มจำนวนสินค้าแล้ว</div>";
}
if($action == 'add'){
    echo "<div class=\"alert alert-success\">เพิ่มสินค้าลงในตะกร้าเรียบร้อยแล้ว</div>";
}
if($action == 'order'){
	echo "<div class=\"alert alert-success\">สั่งซื้อสินค้าเรียบร้อยแล้ว</div>";
}
if($action == 'orderfail'){
	echo "<div class=\"alert alert-warning\">สั่งซื้อสินค้าไม่สำเร็จ มีข้อผิดพลาดเกิดขึ้นกรุณาลองใหม่อีกครั้ง</div>";
}
?>
    <div class="well" style="margin:auto; padding:auto; width:100%;">
		
		<div style="height:20px;"></div>
		<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover">
			<thead>
                <th>ชื่ออุปกรณ์</th>
                <th>ประเภทอุปกรณ์</th>
                <th>จำนวนอุปกรณ์</th>
                <th>สถานะอุปกรณ์</th>
			
				<th>Action</th>
			</thead>
			<tbody>
			<?php

while ($row = mysqli_fetch_array($meQuery)) {

    ?>
					<tr>
                        <td><?php echo $row['product_name']; ?></td>
                        <td><?php echo $row['producttype_name']; ?></td>
                        <td><?php echo $row['product_amount']; ?></td>
                        <td><?php echo $row['product_status']; ?></td>
						
						
						<td>
							<div align="center">

							  <a href="#edit<?php echo $row['product_id']; ?>" data-toggle="modal" class="btn btn-primary btn-xs" ><span class="glyphicon glyphicon-edit"></span>จอง</a> &nbsp;
								
    <div class="modal fade" id="edit<?php echo $row['product_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" enctype="multipart/form-data">
     <div class="modal-dialog modal-lg">
    <div class="modal-content">
     <div class="modal-header">
	<h4 align="center" class="modal-title" id="myModalLabel">การจอง</h4>
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                             <p class="modal-body">
				            <?php

	$edit = mysqli_query($conn, "SELECT pd.*, pt.producttype_name FROM product as pd
    inner JOIN producttype AS pt ON (pd.producttype_id = pt.producttype_id)  WHERE product_id ='" . $row['product_id'] . "'");
	$erow = mysqli_fetch_array($edit);
	
    
    ?>
				<div class="container-fluid">
				<form method="POST" action="../process/process_action.php?p=reservation" enctype="multipart/form-data"> <!-- form -->
				<div class="row">
                    
						<div class="col-lg-2">
							<label style="position:relative; top:7px;">ชื่ออุปกรณ์:</label>
						</div>
						<div class="col-lg-10">
                        <label ><?php echo $erow['product_name']; ?></label>
							<input type="hidden" name="product_name" class="form-control" value="<?php echo $erow['product_id']; ?>"required>
                        </div>
                        <div class="col-lg-2">
							<label style="position:relative; top:7px;">ชื่อประเภทอุปกรณ์:</label>
						</div>
						<div class="col-lg-10">
                            <label ><?php echo $erow['producttype_name']; ?></label>
							<input type="hidden" name="producttype_id" class="form-control" value="<?php echo $erow['producttype_id']; ?>"required>
                        </div>
                        <div class="col-lg-2">
							<label style="position:relative; top:7px;">จำนวนอุปกรณ์ที่จอง:</label>
						</div>
						<div class="col-lg-10">
                            <input type="text" name="product_amount" class="form-control" value="<?php echo $erow['product_amount']; ?>"required>
                        </div>
                        <div class="col-lg-2">
							<label style="position:relative; top:7px;">วันรับอุปกรณ์:</label>
						</div>
						<div class="col-lg-10">
                            <input type="date" name="reservation_receiveday" class="form-control" value="<?php echo $erow['reservation_receiveday']; ?>"required>
                        </div>
                    </div>
                
                <div class="modal-footer">
                    <button type="submit" class="btn btn-warning"><span class="glyphicon glyphicon-check"></span> ยืนยัน</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> ยกเลิก</button>    
                </div>
				</form>
            </div>
        </div>
    </div>
   
   

						</td>
					</tr>
					<?php
}

?>
			</tbody>
		</table>
		</div>
	</div>

</div>


    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
</body>

</html>