<?php session_start(); ?>



<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="album.css">
  
  
</head>
<body>
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <img src="../image/bg2.jpg" width="100%" height="100%">
      </div>
    </div>   
  </div>
 
  


<div class="container">
    	<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-login">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-6">
								<a href="#" class="active" id="login-form-link">เข้าสู่ระบบ</a>
							</div>
							<div class="col-xs-6">
								<a href="#" id="register-form-link">สมัครสมาชิก</a>
							</div>
						</div>
						<hr>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<form id="login-form" action="../config/process.php?p=login" method="post" role="form" style="display: block;">
									<div class="form-group">
										<input type="text" name="member_username" id="username" tabindex="1" class="form-control" placeholder="Username" value="">
									</div>
									<div class="form-group">
										<input type="password" name="member_password" id="password" tabindex="2" class="form-control" placeholder="Password">
									</div>
                           <div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												<input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In">
											</div>
										</div>
									</div>
									
                        </form>
                        

								<form id="register-form" action="../config/process.php?p=register" method="post" role="form" style="display: none;">
									<div class="form-group">
										<input type="text" name="member_username" id="username" tabindex="1" class="form-control" placeholder="ชื่อผู้ใช้" value="">
									</div>
									<div class="form-group">
										<input type="password" name="member_password" id="password" tabindex="2" class="form-control" placeholder="รหัสผ่าน">
									</div>
									<div class="form-group">
										<input type="text" name="member_firstname" id="firstname" tabindex="2" class="form-control" placeholder="ชื่อ">
                           </div>
                           <div class="form-group">
										<input type="text" name="member_lastname" id="lastname" tabindex="1" class="form-control" placeholder="นามสกุล" value="">
                           </div>
                           <div class="form-group">
										<input type="text" name="member_year" id="year" tabindex="1" class="form-control" placeholder="ปีการศึกษา" value="">
                           </div>
                           <div class="form-group" >
                           <select name="member_type" size="1" class="form-control" >                	
                           <option value="">เลือกประเภทผู้ใช้งาน</option>
                           <option value="user">ผู้ใช้งาน</option>
						         <option value="admin">เจ้าหน้าที่</option>
                            </select>
                        </div>
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												<input type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Register Now">
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
   </div>
   
   <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link rel="stylesheet" href="stylesheet.css">

<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script>
$(function() {

$('#login-form-link').click(function(e) {
  $("#login-form").delay(100).fadeIn(100);
   $("#register-form").fadeOut(100);
  $('#register-form-link').removeClass('active');
  $(this).addClass('active');
  e.preventDefault();
});
$('#register-form-link').click(function(e) {
  $("#register-form").delay(100).fadeIn(100);
   $("#login-form").fadeOut(100);
  $('#login-form-link').removeClass('active');
  $(this).addClass('active');
  e.preventDefault();
});

});

</script>



</body>
</html>



  