<?php
include ("menu.php");
//include ("้header.php");

//search
ini_set('display_errors', 1);
error_reporting(~0);

$strKeyword = null;

if (isset($_POST["txtKeyword"])) {
    $strKeyword = $_POST["txtKeyword"];
}
?>
<form style="float:right;" name="frmSearch" method="post" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>">
<div class="form-group" >
			<div class="input-group" >

					<input name="txtKeyword" type="text" id="txtKeyword"  value="<?php echo $strKeyword; ?>">
					<input type="submit" value="Search" class="btn btn-dark"></th>
				
					</div>
			</div>


</form>



<?php

include '../config/config.php';
//search





$sql = "SELECT pd.*, pt.producttype_name FROM product as pd
left JOIN producttype AS pt ON (pd.producttype_id = pt.producttype_id)
WHERE product_name LIKE '%" . $strKeyword . "%'"; 



$query = mysqli_query($conn, $sql);


?>



	<div class="well" style="margin:auto; padding:auto; width:100%;">
		<div style="height:20px;"></div>
		<span class="pull-left"><a href="#addnew" data-toggle="modal" class="btn btn-outline-primary" ><span class="glyphicon glyphicon-plus"></span><i class="fas fa-book-medical"></i> เพิ่มอุปกรณ์ใหม่</a></span>
		<div style="height:20px;"></div>
		<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover">
			<thead>
                <th>ชื่ออุปกรณ์</th>
                <th>ประเภทอุปกรณ์</th>
                <th>จำนวนอุปกรณ์</th>
                <th>Action</th>
				
			</thead>
			<tbody>
			<?php

while ($row = mysqli_fetch_array($query)) {

    ?>
					<tr>
                        <td><?php echo $row['product_name']; ?></td>
                        <td><?php echo $row['producttype_name']; ?></td>
                        <td><?php echo $row['product_amount']; ?></td>
						
						
						<td>
							<div align="center">
                                <a href="product_detail.php" <?php echo $row['product_id']; ?> class="btn btn-success" ><span class="glyphicon glyphicon-edit"></span><i class="fas fa-plus"></i> เพิ่ม</a> &nbsp;
							    <a href="#read<?php echo $row['product_id']; ?>" data-toggle="modal" class="btn btn-primary btn-xs" ><span class="glyphicon glyphicon-edit"></span><i class="fas fa-binoculars"></i> ดู</a> &nbsp;
								<a href="#edit<?php echo $row['product_id']; ?>" data-toggle="modal" class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span><i class="fas fa-edit"></i> แก้ไข</a> &nbsp;
								<a href="#del<?php echo $row['product_id']; ?>" data-toggle="modal" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span><i class="fas fa-trash-alt"></i> ลบ</a> </div>

    <!-- read -->
   
   <div class="modal fade" id="read<?php echo $row['product_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" enctype="multipart/form-data">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header">
					<h4 align="center" class="modal-title" id="myModalLabel">รายละเอียด</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
				<?php
				
            $data = mysqli_query($conn, "SELECT * FROM product WHERE product_id ='" . $row['product_id'] . "'");
            $drow = mysqli_fetch_array($data);
	  ?>
				<div class="container-fluid">
                    <h5><?php echo $drow['product_name'] ?></h5>
                    <h5><?php echo $drow['producttype_name'] ?></h5>
                    <h5><?php echo $drow['product_amount'] ?></h5>
				</div>
				
				<div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> ยกเลิก</button>
                </div>

            </div>
        </div>
    </div>
	</div>
<!-- /.modal -->

	<!-- Delete -->
    <div class="modal fade" id="del<?php echo $row['product_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header">
					<h4 align="center" class="modal-title" id="myModalLabel">ลบอุปกรณ์</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
				<?php
				
    $del = mysqli_query($conn, "SELECT * FROM product WHERE product_id ='" . $row['product_id'] . "'");
    $derow = mysqli_fetch_array($del);
    ?>
				<div class="container-fluid">
					<h5 align="center">ชื่ออุปกรณ์: <strong><?php echo $derow['product_name']; ?></strong></h5>
                </div>
				</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> ยกเลิก</button>
                    <a href="../process/process_product.php?p=delete&product_id=<?php echo $row['product_id']; ?>" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> ลบ</a> <!-- form -->
                </div>

            </div>
        </div>
    </div>
<!-- /.modal -->

<!-- Edit -->
    <div class="modal fade" id="edit<?php echo $row['product_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" enctype="multipart/form-data">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header">
					<h4 align="center" class="modal-title" id="myModalLabel">แก้ไข</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <p class="modal-body">
				<?php

	$edit = mysqli_query($conn, "SELECT * FROM product  WHERE product_id ='" . $row['product_id'] . "'");
	$erow = mysqli_fetch_array($edit);
	
    
    ?>
				<div class="container-fluid">
				<form method="POST" action="../config/process.php?p=edit&product_id=<?php echo $erow['product_id']; ?>" enctype="multipart/form-data"> <!-- form -->
				<div class="row">
						<div class="col-lg-2">
							<label style="position:relative; top:7px;">ชื่ออุปกรณ์:</label>
						</div>
						<div class="col-lg-10">
							<input type="text" name="product_id" class="form-control" value="<?php echo $erow['product_name']; ?>"required>
                        </div>
                        <div class="col-lg-2">
							<label style="position:relative; top:7px;">ประเภทอุปกรณ์:</label>
						</div>
						<div class="col-lg-10">
							<input type="text" name="producttype_id" class="form-control" value="<?php echo $erow['producttype_name']; ?>"required>
                        </div>
                        <div class="col-lg-2">
							<label style="position:relative; top:7px;">จำนวนอุปกรณ์:</label>
						</div>
						<div class="col-lg-10">
							<input type="text" name="product_amount" class="form-control" value="<?php echo $erow['product_amount']; ?>"required>
                        </div>
                    </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> ยกเลิก</button>
                    <button type="submit" class="btn btn-warning"><span class="glyphicon glyphicon-check"></span> บันทึก</button>
                </div>
				</form>
            </div>
        </div>
    </div>
<!-- /.modal -->

						</td>
					</tr>
					<?php
}

?>
			</tbody>
		</table>
		</div>
	</div>

	<!-- Add New -->
    <div class="modal fade" id="addnew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog  modal-lg">
            <div class="modal-content">
                <div class="modal-header">
					<h4 align="center" class="modal-title" id="myModalLabel">เพิ่มอุปกรณ์ใหม่</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

			    <div class="modal-body">
				<div class="container-fluid">
				<!-- form -->
				<form method="POST" action="../process/process_product.php?p=add"  enctype="multipart/form-data">
				<div style="height:10px;"></div>
                <div class="form-group" >
                  <label for="product_name">ชื่ออุปกรณ์</label>
                  <input type="text" name="product_name"class="form-control" id="product_name" placeholder="ชื่ออุปกรณ์" required>
                </div>
                <div class="form-group" >
                  <label for="product_name">ประเภทอุปกรณ์</label>
               
                <?php
                $sqlx = "select * from producttype";
                $ptx = mysqli_query($conn,$sqlx);
                ?>
                <select name ="producttype_id" size="1" class="form-control" required>
                <option value="">เลือกประเภทอุปกรณ์</option>
                <?php
                while($rowprotype = mysqli_fetch_array( $ptx, MYSQLI_ASSOC))
                {
                  echo'<option value="';
                  echo $rowprotype['producttype_id'];
                  echo'">';
                  echo $rowprotype['producttype_name'];

                  echo '</option>';
                }
                ?>
                </select>
                </div>
                <div class="form-group" >
                  <label for="product_name">จำนวนอุปกรณ์</label>
                  <input type="number" name="product_amount" class="form-control" id="product_amount" placeholder="จำนวนอุปกรณ์" required>
                </div>
              <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> ยกเลิก</button>
                    <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> บันทึก</a>
				</form>

                </div>

            </div>
        </div>
    </div>

</div>

    </div>
</div>


        </div>
    </div>
            </div>

    