<?php
 include '../config/config.php';?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>STORE SYSTEM</title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="style4.css">

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>

</head>
<body>

    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">
            <div class="sidebar-header">
            <center><img src="../image/logo.png" width="80" height="150"><center> 
                <h6>ระบบยืม-คืนอุปกรณ์</h6>
                <h6>สาขาวิศวกรรมไฟฟ้า</h6>
                <strong><i class="fas fa-wrench"></i></strong>
            </div>

            <ul class="list-unstyled components">
                <li class="active">
                <p class="name">
                  <?php echo $_SESSION["member_firstname"] ?>
                  </p>
                <li>
                    <a href="product.php">
                        <i class="fas fa-briefcase"></i>
                        หน้าหลัก
                    </a>
                    <li>               
                    <a href="reservation.php">
                        <i class="fas fa-briefcase"></i>
                        การจองอุปกรณ์
                    </a>
                    <a href="#pageSubreport" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <i class="fas fa-copy"></i>
                        รายงานอุปกรณ์
                    </a>
                    <ul class="collapse list-unstyled" id="pageSubreport">
                        <li>
                            <a href="list_reservation.php">รายงานการจอง</a>
                        </li>
                        <li>
                            <a href="list_borrow.php">รายงานการยืม</a>
                        </li>
                        <li>
                            <a href="list_return.php">รายงานการคืน</a>
                        </li>
                        </ul>                       
                </li>
                </ul>
                <ul class="list-unstyled components">
                <a href="../config/process.php?p=logout">
                        <i class="fas fa-briefcase"></i>
                        ออกจากระบบ
                    </a>
                </ul>
        </nav>

        <!-- Page Content  -->
        <div id="content">

            
<div class="content p-4">

<div class="container-fluid">

<!-- jQuery CDN - Slim version (=without AJAX) -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
</body>

</html>