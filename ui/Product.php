<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>store</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="../plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">
   <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
       <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php include("header.php"); ?>
  <?php include("menu.php"); ?>


  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        แสดงข้อมูลอุปกรณ์
      </h1>
      <ol class="breadcrumb">
        <li class="active"><a href="product.add.php"><i class="fa fa-plus-circle"></i>เพิ่มอุปกรณ์ใหม่</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <?php
            include("config.php");
            $query = "SELECT*FROM Product";
              $result = mysqli_query($conn,$query);
            $countx = mysqli_query($conn,$query);
            $count = count(mysqli_fetch_array($countx,MYSQLI_ASSOC));

            if($count > 0){
              ?>

            <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>รหัสอุปกรณ์</th>
                  <th>ชื่ออุปกรณ์</th>
                  <th>จำนวนอุปกรณ์</th>
                  <th>สถานะอุปกรณ์</th>
                  <th>ประเภทอุปกรณ์อุปกรณ์</th>
                  
                <th><i class="fa fa-minus-circle"></i>&nbsp;ลบ</th>
                  <th><i class="fa fa-wrench"></i>&nbsp;แก้ไข</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                  while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
                   echo"<tr><td>".$row['Product_ID']."</td>";
                   echo"<td>".$row['Product_Name']."</td>";
                   echo"<td>".$row['Product_Amount']."</td>";
                   echo"<td>".$row['Product_Status']."</td>";
                   echo"<td>".$row['ProductType_ID']."</td>";
                   echo"<td><a href='process.php?p=deleteP&Product_ID=".$row['Product_ID']."' id='submit' name='submit' class='btn btn-danger '>ลบ</a></td>";
                   echo"<td><a href='product.edit.php?Product_ID=".$row['Product_ID']."'id='submit' name='submit' class='btn btn-warning'>แก้ไข</button></td>";
                   echo"<td></td>";
                   echo"<td></td></tr>";


                  }
                }
             ?>
             </tbody>
            </table>
            
           
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
</body>
</html>
